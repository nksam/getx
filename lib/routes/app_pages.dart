import 'package:get/get.dart';
import 'package:getx_demo/modules/comment/binding/comment_binding.dart';
import 'package:getx_demo/modules/comment/controller/commend_controller.dart';
import 'package:getx_demo/modules/comment/view/commend.dart';
// import 'package:getx_demo/modules/comment/view/commend.dart';
import 'package:getx_demo/modules/home/view/home.dart';

import '../modules/home/bindings/main_binding.dart';
part 'app_routes.dart';

class AppPages {
  const AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => Home(),
      binding: HomeBinding(),
    ),
    GetPage(
        name: _Paths.COMMEND, page: () => Commend(), binding: CommendBinding())
  ];
}
