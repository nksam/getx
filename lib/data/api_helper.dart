import 'package:get/get.dart';


abstract class ApiHelper {
  Future<Response> getPosts();
  Future<Response> getComments();
}

