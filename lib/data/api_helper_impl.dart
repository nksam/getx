import 'dart:async';

import 'package:get/get.dart';

import '../common/constants.dart';
import '../common/storage/storage.dart';
import 'api_helper.dart';

class ApiHelperImpl extends GetConnect with ApiHelper {

  // Hàm này đc gọi ngay khi 1 widget xuất hiện trong bộ nhớ, rất tốt cho việc khởi tạo 1 thứ gì đó cho controller
  @override
  void onInit() {
    httpClient.baseUrl = Constants.baseUrl;
    httpClient.timeout = Constants.timeout;

    addRequestModifier();

    //làm 1 số thử thuật nhẹ với response trước khi nó đc gửi về cho client. Ta có thể bỏ 1 số data dư thừa ko cần thiết
    // để khi data nhận về sẽ sạch sẽ hơn
    // hoặc đơn giản in ra 1 số thông tin ở command line để tiện theo dõi
    httpClient.addResponseModifier((request, response) {
      printInfo(
        info: 'Status Code: ${response.statusCode}\n'
            'Data: ${response.bodyString?.toString() ?? ''}',
      );

      return response;
    });
  }

  //gắn 1 số thứ vào tất cả request
  void addRequestModifier() {
    httpClient.addRequestModifier<dynamic>((request) {
      if (Storage.hasData(Constants.token)) {
        request.headers['Authorization'] = Storage.getValue(Constants.token);
      }

      printInfo(
        info: 'REQUEST ║ ${request.method.toUpperCase()}\n'
            'url: ${request.url}\n'
            'Headers: ${request.headers}\n'
            'Body: ${request.files?.toString() ?? ''}\n',
      );

      return request;
    });
  }

  @override
  Future<Response<dynamic>> getPosts() {
    return get('posts');
  }

  @override
  Future<Response<dynamic>> getComments(){
    return get('posts/1/comments');
  }
}
