import 'package:get/get.dart';
import 'package:getx_demo/modules/home/controllers/main_controller.dart';


class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController());
  }
}
