import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:getx_demo/modules/comment/view/commend.dart';
import 'package:getx_demo/modules/home/controllers/main_controller.dart';
import 'package:getx_demo/routes/app_pages.dart';

class Home extends GetView<HomeController> {
  // HomeController homeController=Get.find();
  Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cái mịa gì thế này"),
      ),
      body: Obx(
        () => ListView.separated(
          separatorBuilder: (context, index) => SizedBox(height: 10),
          itemCount: controller.dataList.length,
          padding: const EdgeInsets.all(16),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final dynamic _data = controller.dataList[index];

            return Container(
              // height: 100,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Title: ${_data['title'].toString()}',
                  ),
                  SizedBox(height: 5),
                  // Text(
                  //   'Body: ${_data['body'].toString()}',
                  // ),
                ],
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // navigator?.push(MaterialPageRoute(builder: (context) => Commend()));
          Get.to(()=>Commend());
        },
        child: Icon(Icons.add),
      ),
    );
  }

  aduDarkQua() {
    return Text("${controller.count} hehe");
  }
}
