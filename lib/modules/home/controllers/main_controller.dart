import 'package:get/get.dart';
import 'package:getx_demo/common/utils/extensions.dart';

import '../../../data/api_helper.dart';

class HomeController extends GetxController {
  final ApiHelper _apiHelper = Get.find();
  var count = RxInt(0);

  //tạo ra 1 list object để hứng data từ API getPosts trả về
  final RxList _dataList = RxList();



  // tạo getter
  List<dynamic> get dataList => _dataList;


  //tạo setter
  set dataList(List<dynamic> dataList) => _dataList.addAll(dataList);


  @override
  void onReady() {
    super.onReady();
    getPosts();
    // getComments();
  }

  void getPosts() {
    _apiHelper.getPosts().futureValue(
          (value) => dataList = value,
          retryFunction: getPosts,
        );
  }



  increment() => count++;
}
