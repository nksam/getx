import 'package:get/get.dart';
import 'package:getx_demo/modules/comment/controller/commend_controller.dart';
import 'package:getx_demo/modules/home/controllers/main_controller.dart';


class CommendBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CommendController());
  }
}
