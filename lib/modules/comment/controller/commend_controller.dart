import 'package:get/get.dart';
import 'package:getx_demo/common/utils/extensions.dart';
import 'package:getx_demo/data/api_helper.dart';

class CommendController extends GetxController {
  //dependency injection, CommendController ko trực tiếp khởi tạo biến của class ApiHelper
  // mà getX sẽ làm việc này thông qua class Get
  // Việc nó đc khởi tạo sẽ nằm ở file initializer.dart
  // Bằng cách này, thi mọi widget hay class nào extend getX đều có thể truy cập nó
  // thông qua Get.find()
  final ApiHelper _apiHelper = Get.find();

  //Khởi tạo biến

  //tạo ra 1 list object để hứng data từ API getComments trả về
  final RxList _commendList = RxList();

  //Tạo getter
  List<dynamic> get commentList => _commendList;

  //Tạo setter
  set commentList(List<dynamic> commentList) =>
      _commendList.addAll(commentList);

  void getComments() {
    _apiHelper.getComments().futureValue(
        (value) => {print("Thế cơ ak"), commentList = value},
        retryFunction: getComments);
  }

  // nơi mà ta có thể thoải mái side effect
  @override
  void onReady() {
    super.onReady();
    getComments();
  }
}
