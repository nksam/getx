import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:getx_demo/modules/comment/controller/commend_controller.dart';
import 'package:getx_demo/routes/app_pages.dart';

class Commend extends GetView<CommendController> {
  Commend({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ôi gì thế này"),
      ),
      body: Obx(() => ListView.separated(
            separatorBuilder: (context, index) => SizedBox(height: 10),
            itemCount: controller.commentList.length,
            padding: const EdgeInsets.all(16),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              final dynamic _data = controller.commentList[index];

              return Container(
                // height: 100,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Name: ${_data['name'].toString()}',
                    ),
                    SizedBox(height: 15),
                    // Text(
                    //   'Body: ${_data['body'].toString()}',
                    // ),
                  ],
                ),
              );
            },
          )),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Get.toNamed(AppPages.routes[0].name);
        },
        child: Text("Nhấn"),
      ),
    );
  }
}
